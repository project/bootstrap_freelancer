<?php
/**
 * @file
 * bootstrap_freelancer_common.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bootstrap_freelancer_common_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'portfolio_grid_section';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Portfolio';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Portfolio block';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Portfolio Image */
  $handler->display->display_options['fields']['field_portfolio_image']['id'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['table'] = 'field_data_field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['field'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['text'] = '[field_portfolio_image]';
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['preserve_tags'] = '<img>';
  $handler->display->display_options['fields']['field_portfolio_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_portfolio_image']['settings'] = array(
    'url_type' => '0',
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="col-sm-4 portfolio-item">
		<a href="#portfolioModal[nid]" class="portfolio-link" data-toggle="modal">
				<div class="caption">
						<div class="caption-content">
								<i class="fa fa-search-plus fa-3x"></i>
						</div>
				</div>
				<img src="[field_portfolio_image]" class="img-responsive" alt="">
		</a>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'portfolio' => 'portfolio',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Portfolio</h2>
                    <hr class="star-primary">
                </div>
            </div>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';

  /* Display: Popup content */
  $handler = $view->new_display('block', 'Popup content', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Portfolio Image */
  $handler->display->display_options['fields']['field_portfolio_image']['id'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['table'] = 'field_data_field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['field'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['text'] = '[field_portfolio_image]';
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_portfolio_image']['alter']['preserve_tags'] = '<img>';
  $handler->display->display_options['fields']['field_portfolio_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_portfolio_image']['element_class'] = 'img-responsive';
  $handler->display->display_options['fields']['field_portfolio_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_portfolio_image']['settings'] = array(
    'url_type' => '0',
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="portfolio-modal modal fade" id="portfolioModal[nid]" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-content">
				<div class="close-modal" data-dismiss="modal">
						<div class="lr">
								<div class="rl">
								</div>
						</div>
				</div>
				<div class="container">
						<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
										<div class="modal-body">
												<h2>[title]</h2>
												<hr class="star-primary">
												<img src="[field_portfolio_image]" class="img-responsive" alt="">
												[body]
										</div>
								</div>
						</div>
				</div>
		</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $export['portfolio_grid_section'] = $view;

  return $export;
}
