<?php
/**
 * Add body classes if certain regions have content.
 */
function freelancer_bootstrap_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }
  
  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }
}

function freelancer_bootstrap_form_alter(&$form, &$form_state, $form_id)
{
  if ($form_id == 'webform_client_form_7') {
    $form['actions']['submit']['#attributes']['class'][] = 'btn btn-success btn-lg'; 
		$form['actions']['submit']['#prefix'] = '<div class="row"><div class="form-group col-xs-12">';
		$form['actions']['submit']['#suffix'] = '</div></div>';
  }
}

function freelancer_bootstrap_preprocess_page(&$vars) {
  if (drupal_is_front_page()) {
    unset($vars['page']['content']['system_main']['default_message']); //will remove message "no front page content is created"
    drupal_set_title(''); //removes welcome message (page title)
  }
}