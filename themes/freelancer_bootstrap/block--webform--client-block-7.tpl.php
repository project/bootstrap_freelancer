<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
	<section id="contact">
		<div class="container">
				<div class="row">
						<div class="col-lg-12 text-center">
								<?php print render($title_prefix); ?>
								<?php if ($block->subject): ?>
									<h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
								<?php endif;?>
								<hr class="star-primary">
						</div>
				</div>
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<?php print render($title_suffix); ?>

						<div class="content"<?php print $content_attributes; ?>>
							<?php print $content ?>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>